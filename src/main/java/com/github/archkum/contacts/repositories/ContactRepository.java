package com.github.archkum.contacts.repositories;

import com.github.archkum.contacts.models.Contact;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by amit on 9/18/2016.
 */
public interface ContactRepository extends CrudRepository<Contact, Long> {
    List<Contact> findByPhone(String phone);
}

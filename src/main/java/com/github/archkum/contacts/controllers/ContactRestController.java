package com.github.archkum.contacts.controllers;

import com.github.archkum.contacts.models.Contact;
import com.github.archkum.contacts.repositories.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * Created by amit on 9/18/2016.
 */


@RestController
@RequestMapping("/contacts")
public class ContactRestController {

    @Autowired
    private ContactRepository repository;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<Contact>> getAllContacts(){
        return new ResponseEntity<>((Collection<Contact>) repository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Contact> getContactWithId(@PathVariable Long id) {
        Contact contact = repository.findOne(id);
        if(contact == null) {
            throw new ContactNotFoundException(id);
        }
        return new ResponseEntity<>(contact,HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, params = {"phone"})
    public ResponseEntity<Collection<Contact>> findContactWithPhone(@RequestParam(value="phone") String phone) {
        return new ResponseEntity<>(repository.findByPhone(phone), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addContact(@RequestBody @Valid Contact input) {
        return new ResponseEntity<>(repository.save(input), HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<?> updateContact(@RequestBody @Valid Contact input, @PathVariable Long id) {
        Contact newContact = new Contact(id, input.getFirst_name(), input.getLast_name(), input.getPhone(), input.getEmail());
        return new ResponseEntity<>(repository.save(newContact), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteContact(@PathVariable Long id) {
        repository.delete(id);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    class ContactNotFoundException extends RuntimeException {

        public ContactNotFoundException(Long userId) {
            super("could not find user '" + userId + "'.");
        }
    }
}
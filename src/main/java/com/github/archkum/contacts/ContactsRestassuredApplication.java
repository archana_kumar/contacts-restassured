package com.github.archkum.contacts;

import com.github.archkum.contacts.models.Contact;
import com.github.archkum.contacts.repositories.ContactRepository;
import com.github.javafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ContactsRestassuredApplication {

	private final Faker faker = new Faker();

	public static void main(String[] args) {
		SpringApplication.run(ContactsRestassuredApplication.class, args);
	}

	@Bean
	public CommandLineRunner initializeDb(ContactRepository repository){
		return (args) -> {
			repository.deleteAll();
			//Insert some random pies
			for(int i = 0; i < 20; i++) {
				repository.save(new Contact(faker.name().firstName(),
						faker.name().lastName(),
						faker.phoneNumber().cellPhone(),
						faker.internet().emailAddress()));
			}
		};
	}

}

